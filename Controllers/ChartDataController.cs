﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using Newtonsoft.Json;
using System.IO;
using RiverChartDataRequestAndProcessLibrary;
using System.Web.Configuration;
using System.Text;
using RiverChartNWWebApplication.Models;

namespace RiverChartNWWebApplication.Controllers
{
    public class ChartDataController : ApiController
    {
        static readonly IChartDataRepository repository = new ChartDataRepository();
        //public ChartData GetChartDatas(string stationID, string PEType)
        //{
        //    return repository.Get(stationID, PEType);
        //}
        public HttpResponseMessage GetChartDatas(string stationID, string PEType)
        {
            //string p1=System.Web.Hosting.HostingEnvironment.MapPath("~");

            bool readed = false;
            int maxTries = 10;
            string fileContents = string.Empty;
            string dataFilePath = string.Format("{0}{1}_{2}", WebConfigurationManager.AppSettings["SavedDataDirectory"], stationID, PEType);
            if (File.Exists(dataFilePath))
            {
                while (!readed && maxTries > 0)
                {
                    try
                    {
                        Console.WriteLine("Reading...");
                        using (FileStream fs = new FileStream(dataFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            //return sr.ReadToEnd();
                            while (!sr.EndOfStream)
                                fileContents += sr.ReadToEnd();
                        }
                        readed = true;
                        Console.WriteLine("Readed");
                    }
                    catch (IOException)
                    {
                        Console.WriteLine("Fail: " + maxTries.ToString());
                        maxTries--;
                        Thread.Sleep(1000);
                    }
                }

                Thread countAndSaveThread = new Thread(UpdateCountAndSave.UpdateAndSave);
                CountAndSaveData data = new CountAndSaveData() { stationId = stationID, peType = PEType, data = string.Empty };
                countAndSaveThread.Start(data);

                var response = this.Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(fileContents, Encoding.UTF8, "application/json");
                return response;
            }
            else
            {
                // get file and return
                var response = this.Request.CreateResponse(HttpStatusCode.OK);
                string chartDataString = JsonConvert.SerializeObject(repository.Get(stationID, PEType));
                response.Content = new StringContent(chartDataString, Encoding.UTF8, "application/json");

                Thread countAndSaveThread = new Thread(UpdateCountAndSave.UpdateAndSave);
                CountAndSaveData data = new CountAndSaveData() { stationId = stationID, peType = PEType, data = chartDataString };
                countAndSaveThread.Start(data);

                return response;
            }
        }

        public ChartData GetChartDatas(string stationID, string PEType, DateTime logDate)
        {
            DateTime startDate = logDate.Subtract(new TimeSpan(3, 0, 0, 0));
            DateTime endDate = logDate.AddDays(3);
            startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
            endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0);
            return RiverChartDataRequestAndProcessLibrary.GetChartDataFromObservedData.GetForDates(stationID, PEType, startDate, endDate, logDate);
        }

    }
}
