﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RiverChartDataRequestAndProcessLibrary;

namespace RiverChartNWWebApplication.Controllers
{
    public class DataItemController : ApiController
    {
        static readonly IDataItemRepository repository = new DataItemRepository();

        public IEnumerable<SimpleDataItem> GetDataItems(string searchString)
        {
            return repository.Get(searchString, "all");
        }
        public IEnumerable<SimpleDataItem> GetDataItems()
        {
            return repository.Get();
        }
    }
}
