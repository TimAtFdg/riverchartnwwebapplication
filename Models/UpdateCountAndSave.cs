﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RiverChartDataRequestAndProcessLibrary;
using System.Web.Configuration;
using System.IO;

namespace RiverChartNWWebApplication.Models
{
    public class UpdateCountAndSave
    {
        public static void UpdateAndSave(object countAndSaveDataObject)
        {
            CountAndSaveData countAndSaveData = (CountAndSaveData)countAndSaveDataObject;
            UpdateCount updateCount = new UpdateCount();
            updateCount.IncrementCountFor(countAndSaveData.stationId, countAndSaveData.peType);

            if (countAndSaveData.data != string.Empty)
            {
                string dataFilePath = string.Format("{0}{1}_{2}", WebConfigurationManager.AppSettings["SavedDataDirectory"], countAndSaveData.stationId, countAndSaveData.peType);
                using (FileStream fs = new FileStream(dataFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.Write(countAndSaveData.data);
                    }
                }
            }
        }
    }
}