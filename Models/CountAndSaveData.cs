﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiverChartNWWebApplication.Models
{
    public class CountAndSaveData
    {
        public string stationId { get; set; }
        public string peType { get; set; }
        public string data { get; set; }
    }
}